/**
 * 格式化时间戳为易于观看时间
 * @return {string}
 */
function TimeFormat(timestamp) {
    if (timestamp === null) {
        return ""
    }
    var date = new Date(parseInt(timestamp) * 1000);
    var year = date.getFullYear(),
        month = ("0" + (date.getMonth() + 1)).slice(-2),
        sdate = ("0" + date.getDate()).slice(-2),
        hour = ("0" + date.getHours()).slice(-2),
        minute = ("0" + date.getMinutes()).slice(-2),
        second = ("0" + date.getSeconds()).slice(-2);
    return year + "-" + month + "-" + sdate + " " + hour + ":" + minute + ":" + second;
}

/**
 * 格式化状态为字符串格式
 * @return {string}
 */
function StatusFormat(status) {
    if (status === 0)
        return '禁用';
    else
        return '可用';
}

/**
 * 格式化状态为HTML格式
 * @return {string}
 */
function StatusFormatHTML(status) {
    if (status === 0)
        return '<i class="layui-icon layui-icon-close-fill" style="font-size: 20px; color: #FF0000;"></i>';
    else
        return '<i class="layui-icon layui-icon-ok-circle" style="font-size: 20px; color: #0070FF;"></i>';
}

/**
 * 格式化登录日志类型
 * @return {string}
 */
function LogTypeFormat(status) {
    if (status === 1)
        return '后台登录';
    else
        return 'APP登录';
}

/**
 * 格式化登录日志状态
 * @return {string}
 */
function StatusFormatHTML(status) {
    if (status === 2)
        return '<i class="layui-icon layui-icon-close-fill" style="font-size: 20px; color: #FF0000;"></i>';
    else
        return '<i class="layui-icon layui-icon-ok-circle" style="font-size: 20px; color: #0070FF;"></i>';
}

// 刷新页面
function ReloadLists() {
    setTimeout(function () {
        xadmin.father_reload();
        xadmin.close();
    }, 1000)
}

function FormatMenu(menus) {
    var data = {};
    var index = 0;
    for(i=0;i<menus.length;i++){
        for(j=0;j<menus[i].children.length;j++){
            for(k=0;k<menus[i].children[j].children.length;k++){
                var temp = {};
                temp.controller = menus[i].children[j].children[k].controller
                temp.function = menus[i].children[j].children[k].function
                temp.level1 = menus[i].children[j].children[k].level1
                temp.level2 = menus[i].children[j].children[k].level2
                temp.level3 = menus[i].children[j].children[k].level3
                data[index++] = temp
            }
        }
    }
    return data;
}