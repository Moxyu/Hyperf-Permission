<?php

return [

    // 后台首页
    'index'       => [
        'name'  => '后台首页',
        'icon'  => '&#xe6d7;',
        'nodes' => [
            'index' => [
                'name'   => '我的桌面',
                'method' => [
                    ['name' => '我的桌面', 'function' => 'desktop'],
                ],
            ],
            'password' => [
                'name'   => '修改密码',
                'method' => [
                    ['name' => '修改密码页面', 'function' => 'index'],
                    ['name' => '修改密码', 'function' => 'update'],
                ],
            ],
            'cache' => [
                'name'   => '清除缓存',
                'method' => [
                    ['name' => '清除缓存页面', 'function' => 'index'],
                    ['name' => '清除缓存', 'function' => 'clear'],
                ],
            ],
        ],
    ],

    // 权限管理
    'permissions' => [
        'name'  => '权限管理',
        'icon'  => '&#xe726;',
        'nodes' => [
            'admin' => [
                'name'   => '管理员列表',
                'method' => [
                    ['name' => '管理员列表', 'function' => 'index'],
                    ['name' => '管理员添加', 'function' => 'create'],
                    ['name' => '管理员删除', 'function' => 'delete'],
                    ['name' => '管理员修改', 'function' => 'update'],
                ],
            ],
            'role'  => [
                'name'   => '角色列表',
                'method' => [
                    ['name' => '角色列表', 'function' => 'index'],
                    ['name' => '角色添加', 'function' => 'create'],
                    ['name' => '角色删除', 'function' => 'delete'],
                    ['name' => '角色修改', 'function' => 'update'],
                ],
            ],
        ],
    ],

    // 日志管理
    'log'         => [
        'name'  => '日志管理',
        'icon'  => '&#xe811;',
        'nodes' => [
            'log' => [
                'name'   => '后台登录日志',
                'method' => [
                    ['name' => '后台登录日志页面', 'function' => 'admin'],
                ],
            ]
        ],
    ],

    // 系统管理
    'system'      => [
        'name'  => '系统管理',
        'icon'  => '&#xe6ae;',
        'nodes' => [
            'system' => [
                'name'   => '系统管理',
                'method' => [
                    ['name' => '系统信息查看', 'function' => 'index'],
                    ['name' => '系统信息修改', 'function' => 'save'],
                ],
            ]
        ],
    ],
];