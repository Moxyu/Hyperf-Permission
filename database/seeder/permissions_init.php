<?php

declare(strict_types=1);

use App\Model\Permission;
use Hyperf\Database\Seeders\Seeder;

class PermissionsInit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //删除原来菜单
        Permission::query()->where("role_id", 1)->delete();
        //创建初始化权限
        $menu = config('menus');
        foreach ($menu as $k1 => $v1) {
            foreach ($v1['nodes'] as $k2 => $v2) {
                foreach ($v2['method'] as $k3 => $v3) {
                    $data['role_id'] = 1;
                    $data['level1'] = $v1['name'];
                    $data['level2'] = $v2['name'];
                    $data['level3'] = $v3['name'];
                    $data['controller'] = $k2;
                    $data['method'] = $v3['function'];
                    Permission::query()->create($data);
                }
            }
        }
    }
}
