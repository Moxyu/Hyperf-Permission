<?php

declare(strict_types=1);

use Hyperf\Database\Seeders\Seeder;

class RoleInit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Role::query()->firstOrCreate(['name' => "超级管理员"], [
            "id"       => 1,
            "name"     => "超级管理员",
            "describe" => "超级管理员(不可删除)",
            "status"   => 1,
        ]);
    }
}
