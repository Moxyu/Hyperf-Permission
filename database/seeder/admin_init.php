<?php

declare(strict_types=1);

use Hyperf\Database\Seeders\Seeder;

class AdminInit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Admin::query()->firstOrCreate(['username' => "admin"], [
            "id"              => 1,
            "role_id"         => 1,
            "username"        => "admin",
            "password"        => \App\Utils\StrHash::hash("admin"),
            "mobile"          => "15335300310",
            "email"           => "1094111567@qq.com",
            "login_num"       => 0,
            "last_login_time" => time(),
            "last_login_ip"   => "127.0.0.1",
            "status"          => 1,
        ]);
    }
}
