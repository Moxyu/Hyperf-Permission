<?php

declare(strict_types=1);

use Hyperf\Database\Seeders\Seeder;

class SystemInit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'group' => 'Site',
                'name'  => 'site_name',
                'value' => '站点名称',
            ],
            [
                'group' => 'Site',
                'name'  => 'site_logo',
                'value' => '站点Logo',
            ],
            [
                'group' => 'Site',
                'name'  => 'site_url',
                'value' => '站点url',
            ],
            [
                'group' => 'Site',
                'name'  => 'site_keyword',
                'value' => '站点关键词',
            ],
            [
                'group' => 'Site',
                'name'  => 'site_describe',
                'value' => '站点描述',
            ],
            [
                'group' => 'Site',
                'name'  => 'site_mobile',
                'value' => '站点联系方式',
            ],
        ];
        foreach ($data as $row) {
            \App\Model\System::query()->firstOrCreate(['name' => $row['name']], $row);
        }
    }
}
