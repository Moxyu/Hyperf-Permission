<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreateAdminLoginLogsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('admin_login_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('admin_id')->comment('登录的后台用户ID');
            $table->string('username')->comment('登录用户名');
            $table->string('password')->comment('登录密码');
            $table->tinyInteger('type')->comment('登录类型 1后台登录,2APP登录');
            $table->tinyInteger('status')->comment('登录状态 1成功,2失败');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('admin_login_logs');
    }
}
