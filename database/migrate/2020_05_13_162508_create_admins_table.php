<?php

use Hyperf\Database\Migrations\Migration;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Schema\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('role_id')->comment('角色ID');
            $table->string('username', 20)->unique()->comment('用户名');
            $table->string('password', 200)->comment('密码');
            $table->string('mobile', 15)->nullable()->comment('手机');
            $table->string('email', 50)->nullable()->comment('邮箱');
            $table->integer('login_num')->default(0)->comment('登录次数');
            $table->string('last_login_time', 20)->nullable()->comment('最后一次登陆时间(时间戳)');
            $table->ipAddress('last_login_ip')->nullable()->comment('最后一次登陆IP');
            $table->tinyInteger('status')->default(1)->comment('状态：1启用,0禁用');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('admins');
    }
}
