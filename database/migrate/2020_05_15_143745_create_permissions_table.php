<?php

use Hyperf\Database\Schema\Schema;
use Hyperf\Database\Schema\Blueprint;
use Hyperf\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('role_id')->comment('角色ID');
            $table->string('level1', 20)->comment('一级名称');
            $table->string('level2', 20)->comment('二级名称');
            $table->string('level3', 20)->comment('三级名称');
            $table->string('controller', 20)->comment('控制器');
            $table->string('method', 20)->comment('方法');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('permissions');
    }
}
