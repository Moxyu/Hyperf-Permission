# 青岛宏大日晟国际贸易有限公司

青岛宏大日晟国际贸易有限公司于2018年创立海螺海淘（中国）海外商品线上直采平台。青岛宏大日晟贸易有限公司包含中日货物进出口，国际货物运输，海淘直采、跨境物流、购物中心为一体的全球优质商品供应链公司。与日本亚马逊签订战略合作协议，中国消费者可通过宏大日晟旗下海螺海淘网直接选购日本亚马逊直营正品，由自营海螺物流阳光清关直邮到家。宏大日晟作为集国际贸易、国际物流为一体的公司，专注于连接全球零售市场与中国本土消费，致力于将世界各地优质丰富的商品以及潮流的生活方式和文化理念同步给中国消费者。

## 系统介绍

本系统采用`Hyperf V1.1.30`开发,如要部署运行必须服务器运行环境要求如下配置

 - PHP >= 7.2
 - Swoole PHP 扩展 >= 4.4，并关闭 `Short Name`
 - OpenSSL PHP 扩展
 - JSON PHP 扩展
 - PDO PHP 扩展
 - Redis PHP 扩展

### 安装使用

- 将根目录的`.env.example`文件重命名`.env`,并修改相应参数

- 进入到项目根目录运行如下命令

```shell script
composer -vvv install
php bin/hyperf.php migrate --path=database/migrate
php bin/hyperf.php db:seed --path=database/seeder
```

### 项目启动

```shell script
php bin/hyperf.php start
```

### 特殊说明
 - 若直接启动请将服务器防火墙以及服务商放行9501端口(Nginx、Apache等需要配置反向代理)
 
 - 创建数据库迁移文件:
   - `php bin/hyperf.php gen:migration create_admins_table --create=admins --path=database/migrate`
   
 - 创建数据库填充文件
   - `php bin/hyperf.php gen:seeder adminInit --path=database/seeder`
    
 - 创建数据库Model文件
   - `php bin/hyperf.php gen:model systems --with-comments`