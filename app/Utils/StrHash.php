<?php

namespace App\Utils;
/**
 * Str和Hash之间工具类
 * @package App\Utils
 */
class StrHash
{
    /**
     * 将Str转换为Hash
     * @param string $str 待转换的字符串
     * @return false|string|null
     */
    public static function hash(string $str)
    {
        return password_hash($str, PASSWORD_DEFAULT);
    }

    /**
     * 验证Str和Hash是否匹配
     * @param string $str Str
     * @param string $hash Hash值
     * @return bool
     */
    public static function very(string $str, string $hash)
    {
        return password_verify($str, $hash);
    }
}