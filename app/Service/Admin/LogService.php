<?php

namespace App\Service\Admin;

use App\Model\AdminLoginLog;
use App\Service\BaseService;

/**
 * 日志服务
 * @package App\Service\Admin
 */
class LogService extends BaseService
{
    /**
     * 获取后台管理员登录日志
     * @param array $request
     * @return array
     */
    public function adminLists(array $request)
    {
        $lists = AdminLoginLog::query()->orderBy("id", "desc");
        if (!isset($request['page']) || empty($request['page'])) {
            $request['page'] = "0";
        }
        if (!isset($request['limit']) || empty($request['limit'])) {
            $request['limit'] = "10";
        }
        if (isset($request['username']) && $request['username'] != '') {
            $lists = $lists->where("username", "like", "%" . $request['username'] . "%");
        }
        if (isset($request['status']) && $request['status'] != '') {
            $lists = $lists->where("status", $request['status']);
        }
        $data['count'] = $lists->count();
        $data['lists'] = $lists->skip(($request['page'] - 1) * $request['limit'])->take($request['limit'])->get();
        return $this->baseResponse(0, "获取后台登录日志列表成功", $data);
    }
}