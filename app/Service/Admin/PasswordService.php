<?php

namespace App\Service\Admin;

use App\Model\Admin;
use App\Service\BaseService;
use App\Utils\StrHash;
use Hyperf\Contract\SessionInterface;
use Hyperf\Di\Annotation\Inject;

/**
 * 密码服务
 * @package App\Service\Admin
 */
class PasswordService extends BaseService
{
    /**
     * @Inject()
     * @var SessionInterface
     */
    protected $session;

    /**
     * 修改密码
     * @param array $request 当前请求
     * @param Admin $admin 当前登录用户
     * @return array
     */
    public function update(array $request, Admin $admin)
    {
        if (!StrHash::very($request["oldPassword"], $admin['password'])) {
            return $this->baseResponse(1, "旧密码校验不通过");
        }
        $admin['password'] = StrHash::hash($request["newPassword"]);
        $admin->save();
        $this->session->remove("admin");
        return $this->baseResponse(0, "密码修改成功");
    }
}