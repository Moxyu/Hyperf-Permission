<?php

namespace App\Service\Admin;

use App\Model\System;
use App\Service\BaseService;

/**
 * 系统服务
 * @package App\Service\Admin
 */
class SystemService extends BaseService
{
    /**
     * 根据配置组获取配置信息
     * @param array $groups 配置组
     * @return array
     */
    public function getConfig(array $groups)
    {
        $data = System::query()->whereIn('group', $groups)->get();
        foreach ($data as $row) {
            $configs[$row['name']] = $row['value'];
        }
        return isset($configs) ? $configs : [];
    }

    /**
     * 配置保存
     * @param array $request 请求信息
     * @return array
     */
    public function save(array $request)
    {
        foreach ($request as $k => $v) {
            System::query()->where('name', $k)->update(['value' => $v]);
        }
        return $this->baseResponse(0, '保存配置成功');
    }
}