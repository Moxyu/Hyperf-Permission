<?php

namespace App\Service\Admin;

use App\Model\Permission;
use App\Model\Role;
use App\Service\BaseService;
use Hyperf\Database\Model\Builder;
use Hyperf\Database\Model\Collection;
use Hyperf\Database\Model\Model;
use Hyperf\DbConnection\Db;

class RoleService extends BaseService
{
    /**
     * 获取角色列表
     * @param array $request
     * @return array
     */
    public function lists(array $request)
    {
        $lists = Role::query()->orderBy("id", "asc");
        if (!isset($request['page']) || empty($request['page'])) {
            $request['page'] = "0";
        }
        if (!isset($request['limit']) || empty($request['limit'])) {
            $request['limit'] = "10";
        }
        if (isset($request['name']) && $request['name'] != '') {
            $lists = $lists->where("name", "like", "%" . $request['name'] . "%");
        }
        $data['count'] = $lists->count();
        $data['lists'] = $lists->skip(($request['page'] - 1) * $request['limit'])->take($request['limit'])->get();
        return $this->baseResponse(0, "获取角色列表成功", $data);
    }

    /**
     * 创建角色
     * @param array $request
     * @return array
     */
    public function create(array $request)
    {
        Db::beginTransaction();
        try {
            $role = Role::query()->create([
                "name"     => $request['name'],
                "describe" => $request['describe'],
                "status"   => $request['status'],
            ]);
            foreach ($request['menus'] as $menu) {
                Permission::query()->create([
                    "role_id"    => $role['id'],
                    "level1"     => $menu['level1'],
                    "level2"     => $menu['level2'],
                    "level3"     => $menu['level3'],
                    "controller" => $menu['controller'],
                    "method"     => $menu['function'],
                ]);
            }
            Db::commit();
            return $this->baseResponse(0, "创建角色成功");
        } catch (\Throwable $ex) {
            Db::rollBack();
            return $this->baseResponse(1, "创建角色失败");
        }
    }

    /**
     * 通过Id获取角色
     * @param int $id 角色ID
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function get(int $id)
    {
        return $this->baseResponse(0, "获取角色成功", Role::query()->find($id));
    }

    /**
     * 通过Id编辑角色
     * @param int $id 角色ID
     * @param array $request
     * @return array
     */
    public function update(int $id, array $request)
    {

        Db::beginTransaction();
        try {
            Role::query()->find($id)->update([
                "name"     => $request['name'],
                "describe" => $request['describe'],
                "status"   => $request['status'],
            ]);
            Permission::query()->where("role_id", $id)->delete();
            foreach ($request['menus'] as $menu) {
                Permission::query()->create([
                    "role_id"    => $id,
                    "level1"     => $menu['level1'],
                    "level2"     => $menu['level2'],
                    "level3"     => $menu['level3'],
                    "controller" => $menu['controller'],
                    "method"     => $menu['function'],
                ]);
            }
            Db::commit();
            return $this->baseResponse(0, "编辑角色成功");
        } catch (\Throwable $ex) {
            Db::rollBack();
            return $this->baseResponse(1, "编辑角色失败");
        }
    }

    /**
     * 通过Id删除角色
     * @param int $id
     * @return array
     */
    public function delete(int $id)
    {
        if ($id == 1) return $this->baseResponse(1, "系统角色无法删除！");
        if (Role::destroy($id)) {
            return $this->baseResponse(0, "删除角色成功");
        }
        return $this->baseResponse(1, "删除角色失败");
    }
}