<?php

namespace App\Service;


/**
 * 基础服务
 * Class LoginService
 * @package App\Service\Admin
 */
class BaseService
{
    /**
     * 基本响应数据
     * @param int $code 响应表示
     * @param string $message 响应内容
     * @param array|object $data 响应数据
     * @return array
     */
    protected function baseResponse(int $code, string $message, $data = null)
    {
        return [
            "code"    => $code,
            "message" => $message,
            "data"    => $data
        ];
    }
}