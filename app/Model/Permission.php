<?php

declare (strict_types=1);
namespace App\Model;

use Carbon\Carbon;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 
 * @property int $role_id 角色ID
 * @property string $level1 一级名称
 * @property string $level2 二级名称
 * @property string $level3 三级名称
 * @property string $controller 控制器
 * @property string $method 方法
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Permission extends Model
{
    /**
     * 表名称
     * @var string
     */
    protected $table = 'permissions';

    /**
     * 字段黑名单
     * @var array
     */
    protected $guarded = [];

    /**
     * 字段类型转换
     * @var array
     */
    protected $casts = ['id' => 'integer', 'role_id' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];

    /**
     * 根据角色Id获取权限列表
     * @param int $role_id 角色ID
     * @return array
     */
    public static function getPermissionsByRoleId(int $role_id)
    {
        $list = self::query()->where("role_id", $role_id)->get()->transform(function ($item) {
            return "/admin/" . $item->controller . "/" . $item->method;
        })->toArray();
        $list[] = "/admin/index/index";
        return $list;
    }

    /**
     * 根据角色Id获取权限名称
     * @param int $role_id 角色ID
     * @param int $level_id 等级
     * @return array
     */
    public static function getPermissionsNameByRoleId(int $role_id, int $level_id)
    {
        return self::query()->where("role_id", $role_id)->select("level{$level_id}")->pluck("level{$level_id}")->toArray();
    }
}