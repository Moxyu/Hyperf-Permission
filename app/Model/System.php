<?php

declare (strict_types=1);
namespace App\Model;

use Carbon\Carbon;
use Hyperf\DbConnection\Model\Model;
/**
 * @property int $id 
 * @property string $group 系统设置组标识
 * @property string $name 系统设置名称
 * @property string $value 系统设置值
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class System extends Model
{
    /**
     * 表名称
     * @var string
     */
    protected $table = 'systems';

    /**
     * 字段黑名单
     * @var array
     */
    protected $guarded = [];

    /**
     * 字段类型转换
     * @var array
     */
    protected $casts = ['id' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}