<?php

declare (strict_types=1);
namespace App\Model;

use Carbon\Carbon;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 
 * @property string $name 角色名称
 * @property string $describe 角色描述
 * @property int $status 状态：1启用,0禁用
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Role extends Model
{
    /**
     * 表名称
     * @var string
     */
    protected $table = 'roles';

    /**
     * 字段黑名单
     * @var array
     */
    protected $guarded = [];

    /**
     * 字段类型转换
     * @var array
     */
    protected $casts = ['id' => 'integer', 'status' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}