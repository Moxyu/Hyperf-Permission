<?php

declare (strict_types=1);
namespace App\Model;

use Carbon\Carbon;
use Hyperf\Database\Model\Relations\BelongsTo;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id 
 * @property int $role_id 角色ID
 * @property string $username 用户名
 * @property string $password 密码
 * @property string $mobile 手机
 * @property string $email 邮箱
 * @property int $login_num 登录次数
 * @property string $last_login_time 最后一次登陆时间(时间戳)
 * @property string $last_login_ip 最后一次登陆IP
 * @property int $status 状态：1启用,0禁用
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Admin extends Model
{
    /**
     * 表名称
     * @var string
     */
    protected $table = 'admins';

    /**
     * 字段黑名单
     * @var array
     */
    protected $guarded = [];

    /**
     * 字段类型转换
     * @var array
     */
    protected $casts = ['id' => 'integer', 'role_id' => 'integer', 'login_num' => 'integer', 'status' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];

    /**
     * 隐藏字段
     * @var string[]
     */
    protected $hidden = ['password'];

    /**
     * 获取角色
     * @return BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }
}