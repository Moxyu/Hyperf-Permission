<?php

declare (strict_types=1);

namespace App\Model;

use Carbon\Carbon;
use Hyperf\DbConnection\Model\Model;

/**
 * @property int $id
 * @property int $admin_id 登录的后台用户ID
 * @property string $username 登录用户名
 * @property string $password 登录密码
 * @property int $type 登录类型 1后台登录,2APP登录
 * @property int $status 登录状态 1成功,2失败
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class AdminLoginLog extends Model
{
    /**
     * 表名称
     * @var string
     */
    protected $table = 'admin_login_logs';

    /**
     * 字段黑名单
     * @var array
     */
    protected $guarded = [];

    /**
     * 字段类型转换
     * @var array
     */
    protected $casts = ['id' => 'integer', 'admin_id' => 'integer', 'type' => 'integer', 'status' => 'integer', 'created_at' => 'datetime', 'updated_at' => 'datetime'];
}