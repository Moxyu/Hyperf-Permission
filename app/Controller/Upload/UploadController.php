<?php

namespace App\Controller\Upload;

use App\Controller\AbstractController;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\RequestMapping;
use Psr\Http\Message\ResponseInterface;

/**
 * 文件上传控制器
 * @Controller()
 * @package App\Controller\Upload
 */
class UploadController extends AbstractController
{
    /**
     * 文件上传
     * @RequestMapping(methods={"GET","POST"},path="/upload")
     * @return ResponseInterface
     */
    public function upload()
    {
        if (!$this->request->hasFile('file')) {
            return $this->baseResponse(1, "请上传文件！");
        }
        $file = $this->request->file("file");
        $filename = time() . rand(0, 9999) . "." . $file->getExtension();
        $file->moveTo(BASE_PATH . "/public/file/" . $filename);
        return $this->baseResponse(0, "上传成功！", [
            "title" => $file->getClientFilename(),
            "src"   => "/file/" . $filename,
            "url"   => "/file/" . $filename
        ]);
    }
}