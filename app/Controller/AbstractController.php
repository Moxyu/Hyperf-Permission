<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://doc.hyperf.io
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf-cloud/hyperf/blob/master/LICENSE
 */

namespace App\Controller;

use Hyperf\Contract\SessionInterface;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Hyperf\View\RenderInterface;
use Psr\Container\ContainerInterface;

abstract class AbstractController
{
    /**
     * @Inject
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @Inject
     * @var RequestInterface
     */
    protected $request;

    /**
     * @Inject
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @Inject()
     * @var RenderInterface
     */
    protected $render;

    /**
     * @Inject()
     * @var ValidatorFactoryInterface
     */
    protected $validation;

    /**
     * @Inject()
     * @var SessionInterface
     */
    protected $session;

    /**
     * 基本响应数据
     * @param int $code 响应表示
     * @param string $message 响应内容
     * @param array|object $data 响应数据
     * @param int $http_status 响应http状态码
     * @param array $headers 响应Header
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function baseResponse(int $code, string $message, $data = null, int $http_status = 200, $headers = [])
    {
        $response = $this->response->json([
            "code"    => $code,
            "message" => $message,
            "data"    => $data
        ])->withStatus($http_status);
        foreach ($headers as $k => $v) {
            $response = $response->withHeader($k, $v);
        }
        return $response;
    }
}
