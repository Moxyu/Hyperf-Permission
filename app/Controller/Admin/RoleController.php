<?php

namespace App\Controller\Admin;

use App\Controller\AbstractController;
use App\Model\Permission;
use App\Service\Admin\RoleService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Psr\Http\Message\ResponseInterface;

/**
 * 角色控制器
 * @Controller()
 * @package App\Controller\Admin
 */
class RoleController extends AbstractController
{
    /**
     * @Inject()
     * @var RoleService
     */
    protected $roleService;

    /**
     * 角色列表页面
     * @GetMapping("index")
     * @return ResponseInterface
     */
    public function index()
    {
        return $this->render->render("admin.role.index");
    }

    /**
     * 角色列表
     * @PostMapping("index")
     * @return ResponseInterface
     */
    public function lists()
    {
        $data = $this->roleService->lists($this->request->all());
        return $this->baseResponse($data['code'], $data['message'], $data['data']);
    }

    /**
     * 角色创建页面
     * @GetMapping("create")
     * @return ResponseInterface
     */
    public function create()
    {
        return $this->render->render("admin.role.create", ["menus" => config("menus")]);
    }

    /**
     * 角色创建
     * @PostMapping("create")
     * @return ResponseInterface
     */
    public function createSubmit()
    {
        $validator = $this->validation->make($this->request->all(), [
            'name'     => 'required|between:2,20',
            'describe' => 'nullable|between:1,30',
            'status'   => 'required|in:0,1',
            'menus'    => 'required',
        ]);
        if ($validator->fails()) {
            return $this->baseResponse(1, $validator->errors()->first());
        }
        $data = $this->roleService->create($this->request->all());
        return $this->baseResponse($data['code'], $data['message'], $data['data']);
    }

    /**
     * 角色修改页面
     * @GetMapping("update/{id}")
     * @param int $id 角色Id
     * @return ResponseInterface
     */
    public function update(int $id)
    {
        $menus = config("menus");
        $roleMenus = Permission::query()->where('role_id', $id)->get()->transform(function ($item) {
            return $item['controller'] . "/" . $item['method'];
        })->toArray();
        $role = $this->roleService->get($id)["data"];
        return $this->render->render("admin.role.update", compact('role', 'menus', 'roleMenus'));
    }

    /**
     * 角色修改
     * @PostMapping("update/{id}")
     * @param int $id 角色Id
     * @return ResponseInterface
     */
    public function updateSubmit(int $id)
    {
        $validator = $this->validation->make($this->request->all(), [
            'name'     => 'required|between:2,20',
            'describe' => 'nullable|between:1,30',
            'status'   => 'required|in:0,1',
            'menus'    => 'required',
        ]);
        if ($validator->fails()) {
            return $this->baseResponse(1, $validator->errors()->first());
        }
        $data = $this->roleService->update($id, $this->request->all());
        return $this->baseResponse($data['code'], $data['message'], $data['data']);
    }

    /**
     * 角色删除
     * @PostMapping("delete/{id}")
     * @param int $id 角色Id
     * @return ResponseInterface
     */
    public function delete(int $id)
    {
        $data = $this->roleService->delete($id);
        return $this->baseResponse($data['code'], $data['message'], $data['data']);
    }
}