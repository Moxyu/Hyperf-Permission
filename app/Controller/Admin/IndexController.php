<?php

namespace App\Controller\Admin;

use App\Controller\AbstractController;
use App\Model\Permission;
use App\Service\Admin\SystemService;
use Hyperf\DbConnection\Db;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\AutoController;

/**
 * 后台首页控制器
 * @AutoController()
 * @package App\Controller\Admin
 */
class IndexController extends AbstractController
{
    /**
     * @Inject()
     * @var SystemService
     */
    protected $systemService;

    /**
     * 后台首页页面
     */
    public function index()
    {
        $permissions = Permission::getPermissionsByRoleId($this->session->get("admin")->id);
        $permissionsName = Permission::getPermissionsNameByRoleId($this->session->get("admin")->id, 1);
        $config = $this->systemService->getConfig(["site"]);
        return $this->render->render("admin.index.index", [
            "menus"           => config("menus"),
            "permissions"     => $permissions,
            "permissionsName" => $permissionsName,
            "config"          => $config
        ]);
    }

    /**
     * 我的桌面页面
     */
    public function desktop()
    {
        $admin = $this->session->get("admin");
        $server = [
            "host"            => $this->request->getHeaders()["host"][0],
            "os"              => PHP_OS,
            "os_version"      => php_uname('m'),
            "run_env"         => $this->request->getHeaders()["connection"][0],
            "php_version"     => PHP_VERSION,
            "swoole_version"  => SWOOLE_VERSION,
            "hyperf_version"  => "Console Tool",
            "browser_version" => $this->request->getHeaders()["user-agent"][0],
            "mysql_version"   => Db::selectOne("select version() as version")->version,
            "worker_num"      => config("server.settings.worker_num"),
            "run_pid"         => file_get_contents(config("server.settings.pid_file")),
            "disk_free_space" => round(disk_free_space('/') / 1024 / 1024 / 1024, 5) . "G"
        ];
        return $this->render->render("admin.index.desktop", compact("admin", "server"));
    }
}