<?php

namespace App\Controller\Admin;

use App\Controller\AbstractController;
use App\Model\Role;
use App\Service\Admin\AdminService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Psr\Http\Message\ResponseInterface;

/**
 * 管理员控制器
 * @Controller()
 * @package App\Controller\Admin
 */
class AdminController extends AbstractController
{
    /**
     * @Inject()
     * @var AdminService
     */
    protected $adminService;

    /**
     * 管理员列表页面
     * @GetMapping("index")
     * @return ResponseInterface
     */
    public function index()
    {
        return $this->render->render("admin.admin.index");
    }

    /**
     * 管理员列表
     * @PostMapping("index")
     * @return ResponseInterface
     */
    public function lists()
    {
        $data = $this->adminService->lists($this->request->all());
        return $this->baseResponse($data['code'], $data['message'], $data['data']);
    }

    /**
     * 管理员创建页面
     * @GetMapping("create")
     * @return ResponseInterface
     */
    public function create()
    {
        $roles = Role::query()->select('id', 'name')->get();
        return $this->render->render("admin.admin.create", compact('roles'));
    }

    /**
     * 管理员创建
     * @PostMapping("create")
     * @return ResponseInterface
     */
    public function createSubmit()
    {
        $validator = $this->validation->make($this->request->all(), [
            'role_id'  => 'required|exists:roles,id',
            'username' => 'required|unique:admins,username|between:5,20',
            'password' => 'required|between:5,20',
            'mobile'   => 'nullable|regex:/^1[34578][0-9]{9}$/',
            'email'    => 'nullable|email',
            'status'   => 'required|in:0,1',
        ]);
        if ($validator->fails()) {
            return $this->baseResponse(1, $validator->errors()->first());
        }
        $data = $this->adminService->create($this->request->all());
        return $this->baseResponse($data['code'], $data['message'], $data['data']);
    }

    /**
     * 管理员修改页面
     * @GetMapping("update/{id}")
     * @param int $id 管理员Id
     * @return ResponseInterface
     */
    public function update(int $id)
    {
        $roles = Role::query()->select('id', 'name')->get();
        $admin = $this->adminService->get($id)["data"];
        return $this->render->render("admin.admin.update", compact('admin', 'roles'));
    }

    /**
     * 管理员修改
     * @PostMapping("update/{id}")
     * @param int $id 管理员Id
     * @return ResponseInterface
     */
    public function updateSubmit(int $id)
    {
        $validator = $this->validation->make($this->request->all(), [
            'role_id'  => 'required|exists:roles,id',
            'username' => 'required|unique:admins,username,' . $id . '|between:5,20',
            'password' => 'nullable|between:5,20',
            'mobile'   => 'nullable|regex:/^1[34578][0-9]{9}$/',
            'email'    => 'nullable|email',
            'status'   => 'required|in:0,1',
        ]);
        if ($validator->fails()) {
            return $this->baseResponse(1, $validator->errors()->first());
        }
        $data = $this->adminService->update($id, $this->request->all());
        return $this->baseResponse($data['code'], $data['message'], $data['data']);
    }

    /**
     * 管理员删除
     * @PostMapping("delete/{id}")
     * @param int $id 管理员Id
     * @return ResponseInterface
     */
    public function delete(int $id)
    {
        $data = $this->adminService->delete($id);
        return $this->baseResponse($data['code'], $data['message'], $data['data']);
    }
}