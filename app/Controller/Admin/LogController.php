<?php

namespace App\Controller\Admin;

use App\Controller\AbstractController;
use App\Service\Admin\LogService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Psr\Http\Message\ResponseInterface;

/**
 * 日志控制器
 * @Controller()
 * @package App\Controller\Admin
 */
class LogController extends AbstractController
{
    /**
     * @Inject()
     * @var LogService
     */
    protected $logService;

    /**
     * 管理员登录日志
     * @GetMapping("admin")
     * @return ResponseInterface
     */
    public function admin()
    {
        return $this->render->render("admin.log.admin");
    }

    /**
     * 获取后台管理员登录日志
     * @PostMapping("admin")
     * @return ResponseInterface
     */
    public function adminLists()
    {
        $data = $this->logService->adminLists($this->request->all());
        return $this->baseResponse($data['code'], $data['message'], $data['data']);
    }
}