<?php

namespace App\Controller\Admin;

use App\Controller\AbstractController;
use Hyperf\HttpServer\Annotation\AutoController;
use Psr\Http\Message\ResponseInterface;

/**
 * 缓存控制器
 * @AutoController()
 * @package App\Controller\Admin
 */
class CacheController extends AbstractController
{
    /**
     * 清理缓存页面
     * @return ResponseInterface
     */
    public function index()
    {
        return $this->render->render("admin.cache.index");
    }

    /**
     * 清理缓存
     * @return ResponseInterface
     */
    public function clear()
    {
        $dirs = scandir(BASE_PATH . "/runtime/view");
        foreach ($dirs as $dir) {
            if ($dir != '.' && $dir != '..') {
                unlink(BASE_PATH . "/runtime/view" . '/' . $dir);
            }
        }

        return $this->baseResponse(0, '清理缓存完毕');
    }
}