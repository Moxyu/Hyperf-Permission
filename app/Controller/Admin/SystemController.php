<?php

namespace App\Controller\Admin;

use App\Controller\AbstractController;
use App\Service\Admin\SystemService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\AutoController;
use Psr\Http\Message\ResponseInterface;

/**
 * 系统控制器
 * @AutoController()
 * @package App\Controller\Admin
 */
class SystemController extends AbstractController
{

    /**
     * @Inject()
     * @var SystemService
     */
    protected $systemService;

    /**
     * 系统设置页面
     * @return ResponseInterface
     */
    public function index()
    {
        $data = $this->systemService->getConfig(["site"]);
        return $this->render->render("admin.system.index", compact('data'));
    }

    /**
     * 系统设置保存
     * @return ResponseInterface
     */
    public function save()
    {
        $data = $this->systemService->save($this->request->all());

        return $this->baseResponse($data['code'], $data['message'], $data['data']);
    }
}