<?php

namespace App\Controller\Admin;

use App\Controller\AbstractController;
use App\Service\Admin\PasswordService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\AutoController;
use Psr\Http\Message\ResponseInterface;

/**
 * 密码控制器
 * @AutoController()
 * @package App\Controller\Admin
 */
class PasswordController extends AbstractController
{
    /**
     * @Inject()
     * @var PasswordService
     */
    protected $passwordService;

    /**
     * 修改密码页面
     * @return ResponseInterface
     */
    public function index()
    {
        return $this->render->render("admin.password.index");
    }

    /**
     * 修改密码
     * @return ResponseInterface
     */
    public function update()
    {
        $validator = $this->validation->make($this->request->all(), [
            'oldPassword' => 'required|between:5,20',
            'newPassword' => 'required|between:5,20|confirmed',
        ]);
        if ($validator->fails()) {
            return $this->baseResponse(1, $validator->errors()->first());
        }
        $data = $this->passwordService->update($this->request->all(),$this->session->get("admin"));
        return $this->baseResponse($data['code'], $data['message'], $data['data']);
    }
}