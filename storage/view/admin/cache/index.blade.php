@extends("layout.admin")
@section('body')
    <form action="" class="layui-form">
        <button type="button" class="layui-btn layui-btn-lg layui-btn-normal" lay-submit lay-filter="formDemo">清理缓存</button>
    </form>

    <script>
        layui.use('form', function () {
            var form = layui.form;

            form.on('submit(formDemo)', function (data) {
                $.post("/admin/cache/clear", data.field, function (result) {
                    layer.msg(result.message);
                });
                return false;
            });
        });
    </script>
@endsection