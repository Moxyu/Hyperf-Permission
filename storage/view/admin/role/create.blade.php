@extends("layout.admin")
@section('body')
    <form class="layui-form">
        <div class="layui-form-item">
            <label class="layui-form-label">角色名称<span class="x-red">*</span></label>
            <div class="layui-input-block">
                <input type="text" name="name" lay-verify="required" placeholder="请输入角色名称" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">角色描述</label>
            <div class="layui-input-block">
                <input type="text" name="describe" placeholder="请输入角色描述" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">状态<span class="x-red">*</span></label>
            <div class="layui-input-block">
                <input type="radio" name="status" value="1" title="启用" checked>
                <input type="radio" name="status" value="0" title="禁用">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">角色菜单<span class="x-red">*</span></label>
            <div class="layui-input-block">
                <div id="menus"></div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="submit">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>

    <script>
        var i = 1;
        layui.use(['form', 'tree'], function () {
            var form = layui.form;
            var tree = layui.tree;

            form.on('submit(submit)', function (data) {
                data.field.menus = FormatMenu(tree.getChecked('menus'));
                $.post("/admin/role/create", data.field, function (result) {
                    parent.layer.msg(result.message);
                    if (result.code === 0) {
                        ReloadLists()
                    }
                });
                return false;
            });

            tree.render({
                id: 'menus',
                elem: '#menus',
                showCheckbox: true,
                showLine: true,
                data: [
                        @foreach($menus as $menu1)
                    {
                        id: i++,
                        title: '{{$menu1['name']}}',
                        children: [
                                @foreach($menu1['nodes'] as $controller => $menu2)
                            {
                                id: i++,
                                title: '{{$menu2['name']}}',
                                children: [
                                        @foreach($menu2['method'] as $menu3)
                                    {
                                        id: i++,
                                        title: '{{$menu3['name']}}',
                                        controller: "{{$controller}}",
                                        function: "{{$menu3['function']}}",
                                        level1: "{{$menu1['name']}}",
                                        level2: "{{$menu2['name']}}",
                                        level3: "{{$menu3['name']}}",
                                    },
                                    @endforeach
                                ]
                            },
                            @endforeach
                        ]
                    },
                    @endforeach
                ]
            });
        });
    </script>
@endsection