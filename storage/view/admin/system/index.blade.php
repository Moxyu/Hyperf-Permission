@extends("layout.admin")
@section('body')
    <form class="layui-form " action="">
        <div class="layui-form-item">
            <label class="layui-form-label">站点名称</label>
            <div class="layui-input-block">
                <input type="text" name="site_name" placeholder="请输入旧密码" autocomplete="off" class="layui-input" value="{{$data['site_name']}}">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">站点LOGO</label>
            <div class="layui-input-block">
                <div class="layui-col-md4">
                    <button type="button" class="layui-btn" id="site_logo_but">
                        <i class="layui-icon">&#xe67c;</i>上传Logo
                    </button>
                </div>
            </div>
            <div class="layui-input-block" id="site_logo_view">
                <div class="layui-col-md4">
                    <input class="image-min" type="image" src="{{$data['site_logo']}}" name="site_logo" id="site_logo" value="{{$data['site_logo']}}" onclick='return false;'>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">站点URL</label>
            <div class="layui-input-block">
                <input type="text" name="site_url" placeholder="请输入站点URL" autocomplete="off" class="layui-input" value="{{$data['site_url']}}">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">站点关键字</label>
            <div class="layui-input-block">
                <input type="text" name="site_keyword" placeholder="请输入站点关键字" autocomplete="off" class="layui-input" value="{{$data['site_keyword']}}">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">站点描述</label>
            <div class="layui-input-block">
                <input type="text" name="site_describe" placeholder="请输入站点描述" autocomplete="off" class="layui-input" value="{{$data['site_describe']}}">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">客服电话</label>
            <div class="layui-input-block">
                <input type="text" name="site_mobile" placeholder="请输入客服电话" autocomplete="off" class="layui-input" value="{{$data['site_mobile']}}">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="save">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>

    <script>
        layui.use(['upload', 'form'], function () {
            var upload = layui.upload;
            var form = layui.form;

            form.on('submit(save)', function (data) {
                $.post("/admin/system/save", data.field, function (result) {
                    layer.msg(result.message);
                    if (result.code === 0) {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                });
                return false;
            });

            upload.render({
                elem: '#site_logo_but',
                url: '/upload',
                before: function () {
                    layer.load();
                },
                done: function (res) {
                    layer.closeAll('loading');
                    layer.msg(res.message);
                    if (res.code === 0) {
                        var url = res.data.url;
                        $("#site_logo").attr('src', url);
                        $("#site_logo").attr('value', url);
                    }
                }
            });
        });
    </script>
@endsection