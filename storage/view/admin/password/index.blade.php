@extends("layout.admin")
@section('body')
    <form class="layui-form">
        <div class="layui-form-item">
            <label class="layui-form-label">旧密码</label>
            <div class="layui-input-block">
                <input type="password" name="oldPassword" lay-verify="required" placeholder="请输入旧密码" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">新密码</label>
            <div class="layui-input-block">
                <input type="password" name="newPassword" lay-verify="required" placeholder="请输入新密码" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">重复新密码</label>
            <div class="layui-input-block">
                <input type="password" name="newPassword_confirmation" lay-verify="required" placeholder="请重复输入新密码" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="update">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>

    <script>
        layui.use('form', function () {
            var form = layui.form;

            form.on('submit(update)', function (data) {
                $.post("/admin/password/update", data.field, function (result) {
                    layer.msg(result.message);
                    if (result.code === 0) {
                        setTimeout(function () {
                            xadmin.father_reload();
                        }, 1000)
                    }
                });
                return false;
            });
        });
    </script>
@endsection