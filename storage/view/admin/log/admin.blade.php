@extends("layout.admin")
@section('body')
    <div class="layui-card-body">
        <form class="layui-form" action="">
            <div class="layui-input-inline layui-show-xs-block padding-bottom-10">
                <input class="layui-input" placeholder="用户名" name="username">
            </div>
            <div class="layui-input-inline layui-show-xs-block padding-bottom-10">
                <select name="status">
                    <option value="">登录状态</option>
                    <option value="1">登录成功</option>
                    <option value="2">登录失败</option>
                </select>
            </div>
            <div class="layui-input-inline layui-show-xs-block padding-bottom-10">
                <button class="layui-btn" lay-submit="" lay-filter="sreach">
                    <i class="layui-icon">&#xe615;</i>搜索
                </button>
            </div>
        </form>
        <hr>
        <table id="log" lay-filter="log"></table>
    </div>

    <script>
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var form = layui.form;

            //初始化表格
            table.render({
                elem: '#log',
                method: "post",
                url: '/admin/log/admin',
                page: true,
                cols: [[
                    {field: 'id', title: 'ID'},
                    {field: 'username', title: '用户名'},
                    {field: 'password', title: '密码'},
                    {
                        field: 'type', title: '登录类型', templet: function (d) {
                            return LogTypeFormat(d.type)
                        }
                    },
                    {
                        field: 'status', title: '登录状态', templet: function (d) {
                            return StatusFormatHTML(d.status)
                        }
                    },
                    {field: 'created_at', title: '登录时间'},
                ]],
                parseData: function (res) {
                    return {
                        "code": res.code,
                        "msg": res.message,
                        "count": res.data.count,
                        "data": res.data.lists
                    };
                }
            });

            //搜索
            form.on('submit(sreach)', function (data) {
                table.reload('log', {
                    where: data.field,
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
                return false;
            });
        })
    </script>

    <script type="text/html" id="button">
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
@endsection