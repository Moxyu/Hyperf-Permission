@extends("layout.admin")
@section('body')
    <form class="layui-form">
        <div class="layui-form-item">
            <label class="layui-form-label">角色</label>
            <div class="layui-input-block">
                <select name="role_id" lay-verify="required">
                    <option value="">请选择角色</option>
                    @foreach($roles as $role)
                        <option value="{{ $role['id'] }}" @if($role['id'] == $admin['role_id']) selected @endif>{{ $role['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">用户名<span class="x-red">*</span></label>
            <div class="layui-input-block">
                <input type="text" name="username" lay-verify="required" placeholder="请输入用户名" autocomplete="off"
                       class="layui-input" value="{{$admin['username']}}">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">密码</label>
            <div class="layui-input-block">
                <input type="password" name="password" placeholder="请输入登陆密码(不修改则留空)" autocomplete="off"
                       class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">手机</label>
            <div class="layui-input-block">
                <input type="text" name="mobile" placeholder="请输入手机号" autocomplete="off" class="layui-input" value="{{$admin['mobile']}}">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">邮箱</label>
            <div class="layui-input-block">
                <input type="text" name="email" placeholder="请输入邮箱" autocomplete="off" class="layui-input" value="{{$admin['email']}}">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">状态<span class="x-red">*</span></label>
            <div class="layui-input-block">
                <input type="radio" name="status" value="1" title="启用" @if($admin['status'] == 1) checked @endif>
                <input type="radio" name="status" value="0" title="禁用" @if($admin['status'] == 0) checked @endif>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="submit">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>

    <script>
        layui.use('form', function () {
            var form = layui.form;

            form.on('submit(submit)', function (data) {
                $.post("/admin/admin/update/{{$admin['id']}}", data.field, function (result) {
                    parent.layer.msg(result.message);
                    if (result.code === 0) {
                        ReloadLists()
                    }
                });
                return false;
            });
        });
    </script>
@endsection