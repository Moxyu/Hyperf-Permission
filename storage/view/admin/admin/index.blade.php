@extends("layout.admin")
@section('body')
    <div class="layui-card-body">
        <form class="layui-form" action="">
            <div class="layui-input-inline layui-show-xs-block padding-bottom-10">
                <input class="layui-input" placeholder="用户名" name="username">
            </div>
            <div class="layui-input-inline layui-show-xs-block padding-bottom-10">
                <input class="layui-input" placeholder="手机号" name="mobile">
            </div>
            <div class="layui-input-inline layui-show-xs-block padding-bottom-10">
                <select name="status">
                    <option value="">状态</option>
                    <option value="1">正常</option>
                    <option value="0">禁用</option>
                </select>
            </div>
            <div class="layui-input-inline layui-show-xs-block padding-bottom-10">
                <button class="layui-btn" lay-submit="" lay-filter="sreach">
                    <i class="layui-icon">&#xe615;</i>搜索
                </button>
            </div>
        </form>
        <hr>
        <button class="layui-btn" onclick="xadmin.open('添加角色','/admin/admin/create')">
            <i class="layui-icon"></i>添加管理员
        </button>
        <table id="admin" lay-filter="admin"></table>
    </div>

    <script>
        layui.use(['table', 'form'], function () {
            var table = layui.table;
            var form = layui.form;

            //初始化表格
            table.render({
                elem: '#admin',
                method: "post",
                url: '/admin/admin/index',
                page: true,
                cols: [[
                    {field: 'id', title: 'ID'},
                    {field: 'username', title: '用户名'},
                    {
                        field: 'role_name', title: '角色名称', templet: function (d) {
                            return d.role.name
                        }
                    },
                    {field: 'mobile', title: '手机号'},
                    {field: 'email', title: '邮箱'},
                    {
                        field: 'status', title: '状态', templet: function (d) {
                            return StatusFormatHTML(d.status)
                        }
                    },
                    {
                        field: 'last_login_time', title: '最后登录时间', templet: function (d) {
                            return TimeFormat(d.last_login_time)
                        }
                    },
                    {field: 'created_at', title: '添加时间'},
                    {field: 'updated_at', title: '修改时间'},
                    {field: 'experience', title: '操作', toolbar: '#button', width: 150},
                ]],
                parseData: function (res) {
                    return {
                        "code": res.code,
                        "msg": res.message,
                        "count": res.data.count,
                        "data": res.data.lists
                    };
                }
            });

            //监听表格事件
            table.on('tool(admin)', function (obj) {
                var data = obj.data;
                var layEvent = obj.event;

                if (layEvent === 'del') {
                    layer.confirm('确认删除此行数据吗？', function (index) {
                        layer.close(index);
                        $.post("/admin/admin/delete/" + data.id, {}, function (result) {
                            layer.msg(result.message);
                            if (result.code === 0) {
                                obj.del();
                            }
                        });
                    });
                } else if (layEvent === 'edit') {
                    xadmin.open('编辑角色', '/admin/admin/update/' + data.id)
                }
            });

            //搜索
            form.on('submit(sreach)', function (data) {
                table.reload('admin', {
                    where: data.field,
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                });
                return false;
            });
        })
    </script>

    <script type="text/html" id="button">
        <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    </script>
@endsection