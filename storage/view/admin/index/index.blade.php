<!doctype html>
<html class="x-admin-sm">
<head>
    <meta charset="UTF-8">
    <title>{{$config['site_name']}}-后台管理系统</title>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="stylesheet" href="/css/font.css">
    <link rel="stylesheet" href="/css/xadmin.css">
    <script src="/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/js/xadmin.js"></script>
    <script>
        var is_remember = false;
    </script>
</head>
<body class="index">
<!-- 顶部开始 -->
<div class="container">
    <div class="logo">
        <a href="/admin/index/index">{{$config['site_name']}}后台管理</a></div>
    <div class="left_open">
        <a><i title="展开左侧栏" class="iconfont">&#xe699;</i></a>
    </div>
    <ul class="layui-nav right" lay-filter="">
        <li class="layui-nav-item">
            <a href="javascript:;">admin</a>
            <dl class="layui-nav-child">
                <dd>
                    {{-- TODO:有空看一下个人信息 --}}
                    <a onclick="xadmin.open('个人信息','http://www.baidu.com')">个人信息</a>
                </dd>
                <dd>
                    <a href="/admin/login/logout">切换帐号</a>
                </dd>
                <dd>
                    <a href="/admin/login/logout">退出</a>
                </dd>
            </dl>
        </li>
        <li class="layui-nav-item to-index">
            <a href="/admin/index/index">返回首页</a>
        </li>
    </ul>
</div>
<!-- 顶部结束 -->
<!-- 左侧菜单开始 -->
<div class="left-nav">
    <div id="side-nav">
        <ul id="nav">
            @foreach($menus as $v1)
                @if(in_array($v1['name'],$permissionsName))
                <li>
                    <a href="javascript:;">
                        <i class="iconfont left-nav-li" lay-tips="{{$v1['name']}}">{!! $v1['icon'] !!}</i>
                        <cite>{{$v1['name']}}</cite>
                        <i class="iconfont nav_right">&#xe697;</i>
                    </a>
                    <ul class="sub-menu">
                        @foreach($v1["nodes"] as $k2 => $v2)
                            @if(in_array("/admin/" . $k2 . "/" . $v2['method'][0]["function"],$permissions))
                            <li>
                                <a onclick="xadmin.add_tab('{{$v2['name']}}','{{"/admin/" . $k2 . "/" . $v2['method'][0]["function"]}}')">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>{{$v2['name']}}</cite>
                                </a>
                            </li>
                            @endif
                        @endforeach
                    </ul>
                </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>
<!-- 左侧菜单结束 -->
<!-- 右侧主体开始 -->
<div class="page-content">
    <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
        <ul class="layui-tab-title">
            <li class="home">
                <i class="layui-icon">&#xe68e;</i>我的桌面
            </li>
        </ul>
        <div class="layui-unselect layui-form-select layui-form-selected" id="tab_right">
            <dl>
                <dd data-type="this">关闭当前</dd>
                <dd data-type="other">关闭其它</dd>
                <dd data-type="all">关闭全部</dd>
            </dl>
        </div>
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <iframe src='/admin/index/desktop' frameborder="0" scrolling="yes" class="x-iframe"></iframe>
            </div>
        </div>
        <div id="tab_show"></div>
    </div>
</div>
<div class="page-content-bg"></div>
<style id="theme_style"></style>
<!-- 右侧主体结束 -->
</body>
</html>